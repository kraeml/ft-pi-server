# FT-PI Server

Serverumgebung mit dem [TX-Pi](https://tx-pi.de/de/) (Raspberry Pi mit ft-hat) und dem [ftduino](https://harbaum.github.io/ftduino/www/de/).

![FT-PI-Server mit ftduino Übersicht](./pictures/FT-PI_Server.png)

Möchte man Themen wie IIoT (Industrie 4.0), Hausautomation, Algorithmus, HTML5, Datenbanken, Linux Kenntnisse o.ä. vermitteln, dann bietet die Verbindung von [Raspberry Pi](https://www.raspberrypi.com/products/raspberry-pi-4-model-b/), [Arduino](https://www.arduino.cc/) und
[Fischertechnik](https://www.fischertechnik.de/de-de/) eine für alle Sinne umfassende Spiel- und Lernumgebung (nach dem Motto "Bauen, erleben, begreifen" [@fox_bauen_2020]).

Der PI mit ft-HAT übernimmt im eigenen Netz die Aufgaben eines Cloudservers. Darüber hinaus steuert er Fischertechnik Motoren, Schalter, Sensoren u.ä. digital an. Über I²C können auch Standardsensoren angesteuert werden.

Der Ftduino (Arduino Leonardo Clone) ist ebenfalls für das Ansteuern von Fischertechnik ausgelegt. Im Gegensatz zum Pi bietet er noch analoge Eingänge und ist als Mikrocontroller (MCU) "Echtzeitfähig".

Mit den Fischertechnik Baukästen können elektromechanische pneumatische Simulationen aufgebaut werden.

Das Serversystem wird Schritt für Schritt (Learning by doing) aufgebaut.

## Was wird unbedingt benötigt?

_Zum Einstieg_

* Raspberry Pi 3/4
* ft-Hat
* Netzteil(e) bzw. FT-Akku
* SD-Micro Karte (16GB plus) plus Reader
* PC mit Betriebssystem und Browser (hier Linux/Ubuntu)

_Weiteres_

* Fischertechnik ([TXT Smart Home](https://www.fischertechnik.de/de-de/produkte/spielen/robotics/544624-txt-smart-home) oder [Electropneumatic](https://www.fischertechnik.de/de-de/service/elearning/spielen/txt-electropneumatic) o.ä.)
* Ftduino

Der Pi wird headless via WiFi betreiben. Somit wird kein Monitor und keine Tastatur benötigt.

## Was wird optional noch benötigt?

* Display
* Gehäuse (3D-Drucker oder kaufen)

## Was kommt als nächstes?

[FT-Hat](ft-hat)

_Anmerkung_  
Der FT-PI Server lehnt sich an den Lehrplan der Informatiktechnikerausbildung in Bayern.

https://www.isb.bayern.de/download/20983/fs_lehrplan_informatiktechnik.pdf
