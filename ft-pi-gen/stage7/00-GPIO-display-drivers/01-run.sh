#!/bin/bash -eux

# Install overlays for tft GPIO displays
install -m 755 -g root -o root  LCD-show/waveshare*.dtb ${ROOTFS_DIR}/boot/overlays/
cp -a ${ROOTFS_DIR}/boot/overlays/waveshare35b-v2-overlay.dtb  ${ROOTFS_DIR}/boot/overlays/waveshare35b-v2.dtbo
install -m 755 -g root -o root files/fbcp ${ROOTFS_DIR}/usr/local/bin/fbcp

# Waveshare35b V2: lite orientiation 90
mkdir -p ${ROOTFS_DIR}/usr/share/X11/xorg.conf.d

cp -rf LCD-show/usr/share/X11/xorg.conf.d/99-fbturbo.conf  ${ROOTFS_DIR}/usr/share/X11/xorg.conf.d/99-fbturbo.conf

# Change only someline in config.txt
sed -i -E 's;^#(hdmi_force_hotplug.*);\1;g' ${ROOTFS_DIR}/boot/config.txt
#sed -i -E 's;^#(hdmi_mode.*);\1;g' ${ROOTFS_DIR}/boot/config.txt
#sed -i -E 's;^#(hdmi_drive.*);\1;g' ${ROOTFS_DIR}/boot/config.txt
#sed -i -E 's;^#(hdmi_group)=.*;\1=2;g' ${ROOTFS_DIR}/boot/config.txt
sed -i -E 's;^#(dtparam=i2c_arm=on);\1;g' ${ROOTFS_DIR}/boot/config.txt
sed -i -E 's;^#(dtparam=spi=on);\1;g' ${ROOTFS_DIR}/boot/config.txt
echo "dtoverlay=waveshare35b-v2:rotate=180" >> ${ROOTFS_DIR}/boot/config.txt
#echo "hdmi_mode=87" >> ${ROOTFS_DIR}/boot/config.txt
#echo "hdmi_cvt 480 320 60 6 0 0 0" >> ${ROOTFS_DIR}/boot/config.txt
#echo "display_rotate=1" >> ${ROOTFS_DIR}/boot/config.txt
#ToDo in bullsey only fb0 exists. Why?
sed -i -E 's;(/dev/)fb1;\1fb0;g' ${ROOTFS_DIR}/usr/share/X11/xorg.conf.d/99-fbturbo.conf
