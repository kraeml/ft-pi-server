#!/bin/bash -eux

cp -rf /usr/share/X11/xorg.conf.d/10-evdev.conf /usr/share/X11/xorg.conf.d/45-evdev.conf

cat > /usr/share/X11/xorg.conf.d/99-calibration.conf <<END
Section "InputClass"
    Identifier      "calibration"
    MatchProduct    "ADS7846 Touchscreen"
    Option  "Calibration"   "3696 229 3945 197"
    Option  "SwapAxes"      "0"
EndSection
END
