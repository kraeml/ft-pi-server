#!/bin/bash -eux

# usbmount config
sed -i -E 's;(FS_MOUNTOPTIONS=");\1-fstype=vfat,gid=users,dmask=0007,fmask=0111;g' /etc/usbmount/usbmount.conf

mkdir /etc/systemd/system/systemd-udevd.service.d

cat > /etc/systemd/system/systemd-udevd.service.d/00-my-custom-mountflags.conf <<EOF
[Service]
PrivateMounts=no
EOF

mkdir /home/"${FIRST_USER_NAME}"/workspace/ || true
cd /home/"${FIRST_USER_NAME}"/workspace/
for i in "https://github.com/GrazerComputerClub/Blockly-gPIo.git" 
do
  su - -c "git clone ${i}" "${FIRST_USER_NAME}"
done
