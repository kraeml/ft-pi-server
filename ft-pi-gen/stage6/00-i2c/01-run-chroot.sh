#!/bin/bash -eux

echo 'Install tx-pi Enable I2c'
sed -i -E 's;^#(dtparam=i2c_arm=on);\1;g' /boot/config.txt
sed -i "s/dtparam=i2c_arm=on/dtparam=i2c_arm=on\ndtparam=i2c_vc=on/g" /boot/config.txt
# With cloud-init
# raspi-config nonint do_i2c 0
for i in "avahi-daemon" "raspi-config nonint do_i2c 0" "raspi-config nonint do_spi 0" "raspi-config nonint do_serial 0"
do
  sed -i -E "s/#(\s*- '$i)/\1/g" /boot/user-data
done
## Disable RTC
sed -i -E "s/^(exit 0.*)/\# ack pending RTC wakeup\n\/usr\/sbin\/i2cset -y 0 0x68 0x0f 0x00\n\n\1/g" /etc/rc.local
sed -i -E "s/#(\s*- 'systemctl restart rc-local.service)/\1/g" /boot/user-data
## Power control via GPIO4
echo -e "\ndtoverlay=gpio-poweroff,gpiopin=4,active_low=1" >> /boot/config.txt
