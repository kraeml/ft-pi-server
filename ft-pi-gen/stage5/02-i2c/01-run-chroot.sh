#!/bin/bash -eux

echo 'Install tx-pi Enable I2c'
sed -i -E 's;^#(dtparam=i2c_arm=on);\1;g' /boot/config.txt
sed -i "s/dtparam=i2c_arm=on/dtparam=i2c_arm=on\ndtparam=i2c_vc=on/g" /boot/config.txt
# With cloud-init
# raspi-config nonint do_i2c 0
for i in "avahi-daemon" "raspi-config nonint do_i2c 0" "raspi-config nonint do_spi 0" "raspi-config nonint do_serial 0"
do
  sed -i -E "s/#(\s*- '$i)/\1/g" /boot/user-data
done
