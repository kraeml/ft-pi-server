#!/bin/bash -eux

npm install --global node-red-admin

chown --recursive "${FIRST_USER_NAME}":"${FIRST_USER_NAME}" /home/"${FIRST_USER_NAME}"/.node-red

NODERED_PACKAGES="node-red-dashboard node-red-contrib-bigtimer \
                 node-red-contrib-looptimer \
                 node-red-contrib-solar-power-forecast \
                 node-red-node-openweathermap \
                 node-red-node-arduino node-red-node-pi-gpiod \
                 node-red-contrib-johnny-five \
                 raspi-io"

for i in $NODERED_PACKAGES
do
  su - -c "cd .node-red/ && npm install $i" "${FIRST_USER_NAME}"
done

su - -c 'cd .node-red/ && npm rebuild' "${FIRST_USER_NAME}"

# Pigpiod listen on all interfaces. Do not do this in production.
sed -i -E 's/^(ExecStart.*)-l/\1/g' /lib/systemd/system/pigpiod.service

# Will done via cloudinit
# systemctl enable nodered.service
sed -i -E "s/#(\s*- 'systemctl enable nodered.service)/\1/g" /boot/user-data
sed -i -E "s/#(\s*- 'systemctl enable pigpiod.service)/\1/g" /boot/user-data
sed -i -E "s/#(\s*- 'systemctl start nodered.service)/\1/g" /boot/user-data
sed -i -E "s/#(\s*- 'systemctl start pigpiod.service)/\1/g" /boot/user-data
