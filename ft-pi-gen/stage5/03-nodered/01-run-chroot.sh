#!/bin/bash -eux

mkdir /home/"${FIRST_USER_NAME}"/.node-red || true
chown --recursive "${FIRST_USER_NAME}":"${FIRST_USER_NAME}" /home/"${FIRST_USER_NAME}"/.node-red
