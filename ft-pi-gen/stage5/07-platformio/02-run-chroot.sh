#!/bin/bash -eux

mkdir /home/"${FIRST_USER_NAME}"/.platformio || true
chown --recursive "${FIRST_USER_NAME}":"${FIRST_USER_NAME}" /home/"${FIRST_USER_NAME}"/.platformio/

su - -c "cd /home/${FIRST_USER_NAME}/.platformio && python3 -m venv penv" "${FIRST_USER_NAME}"
su - -c "source /home/${FIRST_USER_NAME}/.platformio/penv/bin/activate && pip install -U platformio" "${FIRST_USER_NAME}"

if [ ! -f /etc/udev/rules.d/99-platformio-udev.rules ]
then
  wget -P /etc/udev/rules.d https://raw.githubusercontent.com/platformio/platformio-core/master/scripts/99-platformio-udev.rules
fi

cat >> /home/${FIRST_USER_NAME}/.bash_aliases <<EOF
alias pio=~/.platformio/penv/bin/pio
alias pio_home="pio home --host=0.0.0.0 --no-open &"
alias platformio=pio
EOF
