#!/bin/bash -eux

mkdir /home/"${FIRST_USER_NAME}"/workspace/ || true
cd /home/"${FIRST_USER_NAME}"/workspace/
for i in "https://github.com/koenvervloesem/raspberry-pi-home-automation.git" \
         "https://github.com/koenvervloesem/Getting-Started-with-ESPHome.git"
do
  su - -c "git clone ${i}" "${FIRST_USER_NAME}"
done
