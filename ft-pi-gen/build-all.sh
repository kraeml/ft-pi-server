#!/bin/bash -e

function debug {
  ls -l /home/vagrant/rpi-gen/pi-gen
}

echo "Checking git submodules ..."
# ToDo Wenn submodule vorhanden, wie kann man dann weiter arbeiten.
if [ ! -d pi-gen ]
then
  git submodule add https://github.com/RPi-Distro/pi-gen.git pi-gen
#else
#  git submodule init
#  git submodule update
fi
touch pi-gen/stage2/SKIP_IMAGES

if [ -d stage6 ]
then
  if [ ! -d stage7/LCD-show ]
  then
    git submodule add https://github.com/waveshare/LCD-show.git stage6/00-GPIO-display-drivers/LCD-show
  #else
  #  git submodule init
  #  git submodule update
  fi
fi

# Insert links from pi-gen
for i in imagetool.sh build.sh export-image export-noobs scripts stage0 stage1 stage2
do
  [ ! -h ./${i} ] && ln -s pi-gen/${i} ./${i}
done
pushd pi-gen
GIT_TAG=$(git rev-list --tags --max-count=1)
git checkout $(git describe --tags ${GIT_TAG})
git branch
for i in $(ls stage?/EXPORT_NOOBS 2>/dev/null)
do
  rm ${i}
done
popd

# GIT_HASH exportieren für /etc/rpi-issue mit ft-pi-server hash
# export GIT_HASH=$(git rev-list --tags --max-count=1)
echo "Building traditional RPI image ..."
./build.sh

echo "All done! Have a look in deploy folder:"
ls -l deploy
